import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  h1style :boolean = false;
  users:object;

  constructor( private data:DataService) { }

  ngOnInit() {
  }

  pss(){
    this.h1style = true
  }
  pss2(){

    this.data.getusers().subscribe(data => {
      this.users=data
      console.log(this.users);
    })

  }
}
